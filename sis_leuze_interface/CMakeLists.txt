cmake_minimum_required(VERSION 3.0.2)
project(sis_leuze_interface)

# if CMAKE_BUILD_TYPE is not specified, take 'Release' as default
IF(NOT CMAKE_BUILD_TYPE)
    SET(CMAKE_BUILD_TYPE Release)
ENDIF(NOT CMAKE_BUILD_TYPE)

## Macro to use c++11 standard (stod: string to double only availble with at least std=c++11)
set (CMAKE_CXX_STANDARD 11)

macro(use_cxx11)
  if (CMAKE_VERSION VERSION_LESS "3.1")
    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11")
    endif ()
  else ()
    set (CMAKE_CXX_STANDARD 11)
  endif ()
endmacro(use_cxx11)

## Find catkin and any catkin packages
find_package(catkin REQUIRED COMPONENTS
             roscpp
             sensor_msgs
             std_msgs
             geometry_msgs
             message_generation
             )

generate_messages(DEPENDENCIES std_msgs geometry_msgs)

## Declare a catkin package
catkin_package()

include_directories(include ${catkin_INCLUDE_DIRS})
