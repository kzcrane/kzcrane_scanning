/**
 * @file /src/qnode.cpp
 *
 * @brief Ros communication central!
 *
 * @date February 2011
 **/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <std_msgs/String.h>
#include <sstream>
#include "../include/qt_ros_ui/qnode.hpp"
#include <octomap_server/xml_make.h>
#include <octomap_server/octomap_run.h>

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace qt_ros_ui {

/*****************************************************************************
** Implementation
*****************************************************************************/
std_msgs::String t_msg;
std_msgs::String t_msg_bak;
bool octo_run_bak=true;
bool t_run=false;

QNode::QNode(int argc, char** argv ) :
	init_argc(argc),
	init_argv(argv)
	{}

QNode::~QNode() {
    if(ros::isStarted()) {
      ros::shutdown(); // explicitly needed since we use ros::start();
      ros::waitForShutdown();
    }
	wait();
}

bool QNode::init() {
	ros::init(init_argc,init_argv,"qt_ros_ui");
	if ( ! ros::master::check() ) {
		return false;
	}
	ros::start(); // explicitly needed since our nodehandle is going out of scope.
	ros::NodeHandle n;
	// Add your ros communications here.
        t_sub = n.subscribe<octomap_server::xml_make>("octomap_serverS/xml_make", 5000, &QNode::callback, this);
        chatter_publisher = n.advertise<std_msgs::String>("chatter", 1000);
	start();
	return true;
}

bool QNode::init(const std::string &master_url, const std::string &host_url) {
	std::map<std::string,std::string> remappings;
	remappings["__master"] = master_url;
	remappings["__hostname"] = host_url;
	ros::init(remappings,"qt_ros_ui");
	if ( ! ros::master::check() ) {
		return false;
	}
	ros::start(); // explicitly needed since our nodehandle is going out of scope.
	ros::NodeHandle n;
        init_nh();
	// Add your ros communications here.
        //start();
        //return true;
        //ros::Rate loop_rate(1);

        t_sub = n.subscribe<octomap_server::xml_make>("octomap_serverS/xml_make", 5000, &QNode::callback, this);
        chatter_publisher = n.advertise<std_msgs::String>("chatter", 1000);


        start();
        return true;
        //ros::spin();
}

bool QNode::init_nh()
{
    ros::NodeHandle n;
    //t_run=false;



    t_sub = n.subscribe<octomap_server::xml_make>("octomap_serverS/xml_make", 5000, &QNode::callback, this);
    chatter_subscriber = n.subscribe<octomap_server::octomap_run>("octomap_serverS/octomap_run_throttle", 5000, &QNode::Callback, this);



}
void QNode::run() {
    ros::NodeHandle n;
    ros::Rate loop_rate(60);//1
    while (ros::ok())
    {
        //t_sub = n.subscribe<octomap_server::xml_make>("octomap_serverS/xml_make", 5000, &QNode::callback, this);
        //chatter_publisher.publish(t_msg);
        //log(Info,std::string("I create: ")+t_msg.data);
        if(t_msg.data!=t_msg_bak.data)
        {
            log(Info,std::string("I sent: ")+t_msg.data);
            t_msg_bak.data=t_msg.data;
        }

        ros::spinOnce();
        loop_rate.sleep();
    }
    std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
    Q_EMIT rosShutdown(); // used to signal the gui for a shutdown (useful to roslaunch)
    /*
	ros::Rate loop_rate(1);
	int count = 0;
	while ( ros::ok() ) {

		std_msgs::String msg;
		std::stringstream ss;
		ss << "hello world " << count;
		msg.data = ss.str();
		chatter_publisher.publish(msg);
		log(Info,std::string("I sent: ")+msg.data);
		ros::spinOnce();
		loop_rate.sleep();
		++count;
        }octomap_
	std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
	Q_EMIT rosShutdown(); // used to signal the gui for a shutdown (useful to roslaunch)
        */
}


void QNode::log( const LogLevel &level, const std::string &msg) {
	logging_model.insertRows(logging_model.rowCount(),1);
	std::stringstream logging_model_msg;
	switch ( level ) {
		case(Debug) : {
				ROS_DEBUG_STREAM(msg);
				logging_model_msg << "[DEBUG] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Info) : {
				ROS_INFO_STREAM(msg);
				logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Warn) : {
				ROS_WARN_STREAM(msg);
				logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Error) : {
				ROS_ERROR_STREAM(msg);
				logging_model_msg << "[ERROR] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Fatal) : {
				ROS_FATAL_STREAM(msg);
				logging_model_msg << "[FATAL] [" << ros::Time::now() << "]: " << msg;
				break;
		}
	}
	QVariant new_row(QString(logging_model_msg.str().c_str()));
        logging_model.setData(logging_model.index(logging_model.rowCount()-1),new_row);
	Q_EMIT loggingUpdated(); // used to readjust the scrollbar
}

void QNode::log_sub( const LogLevel &level, const std::string &msg) {
        logging_model_sub.insertRows(logging_model_sub.rowCount(),1);
        std::stringstream logging_model_msg;
        switch ( level ) {
                case(Debug) : {
                                ROS_DEBUG_STREAM(msg);
                                logging_model_msg << "[DEBUG] [" << ros::Time::now() << "]: " << msg;
                                break;
                }
                case(Info) : {
                                ROS_INFO_STREAM(msg);
                                logging_model_msg << "[INFO] [" <<ros::Time::now() << "]: " << msg;
                                break;
                }
                case(Warn) : {
                                ROS_WARN_STREAM(msg);
                                logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
                                break;
                }
                case(Error) : {
                                ROS_ERROR_STREAM(msg);
                                logging_model_msg << "[ERROR] [" << ros::Time::now() << "]: " << msg;
                                break;
                }
                case(Fatal) : {
                                ROS_FATAL_STREAM(msg);
                                logging_model_msg << "[FATAL] [" << ros::Time::now() << "]: " << msg;
                                break;
                }
        }
        QVariant new_row(QString(logging_model_msg.str().c_str()));
        logging_model_sub.setData(logging_model_sub.index(logging_model_sub.rowCount()-1),new_row);
        Q_EMIT loggingUpdated_sub(); // used to readjust the scrollbar
}

void QNode::callback(const octomap_server::xml_make::ConstPtr &xmlmsg)
{
    //std_msgs::String msg;
    std::stringstream ss;
    t_msg.data = xmlmsg->xmlfile;
    //t_msg.data = ss.str();
    //msg.data=xmlmsg->xmlfile;
    //log(Info,std::string("I create: ")+t_msg.data);

}

void QNode::Callback(const octomap_server::octomap_run::ConstPtr &submsg)
{
    std_msgs::String msg;
    std::stringstream ss;
    ss << "run ";
    msg.data = ss.str();
    log_sub(Info,std::string("ros ")+msg.data);

}
}  // namespace qt_ros_ui
