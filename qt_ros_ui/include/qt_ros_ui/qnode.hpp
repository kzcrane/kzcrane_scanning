/**
 * @file /include/qt_ros_ui/qnode.hpp
 *
 * @brief Communications central!
 *
 * @date February 2011
 **/
/*****************************************************************************
** Ifdefs
*****************************************************************************/

#ifndef qt_ros_ui_QNODE_HPP_
#define qt_ros_ui_QNODE_HPP_

/*****************************************************************************
** Includes
*****************************************************************************/

// To workaround boost/qt4 problems that won't be bugfixed. Refer to
//    https://bugreports.qt.io/browse/QTBUG-22829
#ifndef Q_MOC_RUN
#include <ros/ros.h>
#endif
#include <string>
#include <QThread>
#include <QStringListModel>
#include <octomap_server/xml_make.h>
#include <octomap_server/octomap_run.h>


/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace qt_ros_ui {

/*****************************************************************************
** Class
*****************************************************************************/

class QNode : public QThread {
    Q_OBJECT
public:
	QNode(int argc, char** argv );
	virtual ~QNode();
	bool init();
	bool init(const std::string &master_url, const std::string &host_url);
        bool init_nh();
	void run();
        void callback(const octomap_server::xml_make::ConstPtr &xmlmsg);



	/*********************
	** Logging
	**********************/
	enum LogLevel {
	         Debug,
	         Info,
	         Warn,
	         Error,
	         Fatal
	 };

	QStringListModel* loggingModel() { return &logging_model; }
	void log( const LogLevel &level, const std::string &msg);

        //
        QStringListModel* loggingModel_sub(){return &logging_model_sub;}
        void log_sub(const LogLevel &level, const std::string &msg);
        void Callback(const octomap_server::octomap_run::ConstPtr &submsg);

Q_SIGNALS:
	void loggingUpdated();
    void rosShutdown();

    void loggingUpdated_sub();

private:
	int init_argc;
	char** init_argv;
	ros::Publisher chatter_publisher;
        ros::Subscriber t_sub;

        ros::Subscriber chatter_subscriber;
        QStringListModel logging_model_sub;

    QStringListModel logging_model;
};

}  // namespace qt_ros_ui

#endif /* qt_ros_ui_QNODE_HPP_ */
