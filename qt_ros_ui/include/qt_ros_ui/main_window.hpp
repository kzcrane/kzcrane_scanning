/**
 * @file /include/qt_ros_ui/main_window.hpp
 *
 * @brief Qt based gui for qt_ros_ui.
 *
 * @date November 2010
 **/
#ifndef qt_ros_ui_MAIN_WINDOW_H
#define qt_ros_ui_MAIN_WINDOW_H

/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui/QMainWindow>
#include "ui_main_window.h"
#include "qnode.hpp"
#include <QProcess>

/*****************************************************************************
** Namespace
*****************************************************************************/

namespace qt_ros_ui {

/*****************************************************************************
** Interface [MainWindow]
*****************************************************************************/
/**
 * @brief Qt central, all operations relating to the view part here.
 */
class MainWindow : public QMainWindow {
Q_OBJECT

public:
	MainWindow(int argc, char** argv, QWidget *parent = 0);
	~MainWindow();

	void ReadSettings(); // Load up qt program settings at startup
	void WriteSettings(); // Save qt program settings when closing

	void closeEvent(QCloseEvent *event); // Overloaded function
	void showNoMasterMessage();

        void OnPing();

public Q_SLOTS:
	/******************************************
	** Auto-connections (connectSlotsByName())
	*******************************************/
	void on_actionAbout_triggered();
	void on_button_connect_clicked(bool check );
	void on_checkbox_use_environment_stateChanged(int state);
        void on_pushButton_test_clicked();

    /******************************************
    ** Manual connections
    *******************************************/
    void updateLoggingView(); // no idea why this can't connect automatically
    void updateLoggingView_sub();

    //
    void OnPingEnded();



private:
	Ui::MainWindowDesign ui;
	QNode qnode;

        //
        QProcess mPingProcess;
};

}  // namespace qt_ros_ui

#endif // qt_ros_ui_MAIN_WINDOW_H
