#include <string>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <mc_msgs/readPLC.h>
//#include <odometry_publisher_tutorial/plc_pub_msg.h>


double x = 0.0;
double y = 0.0;
double th = 0.0;
double vx = 0;
double vy = 0;
double vth = 0;
const double degree = M_PI/180;

//
double current_time;// = ros::Time::now();
double last_time;// = ros::Time::now();
double current_x_pos;
double last_time_x_pos;

geometry_msgs::TransformStamped odom_trans;
odom_trans.header.frame_id = "odom";
odom_trans.child_frame_id = "base_link";

//current_time = ros::Time::now();
//last_time = ros::Time::now();

class SubscribeAndPublish
{
private:
  ros::NodeHandle n_;
  ros::NodeHandle nh;
  //ros::NodeHandle private_nh("~");
  ros::Publisher odom_pub;
  ros::Subscriber sub_;
  ros::Subscriber sub2_;

public:
  SubscribeAndPublish()
  {
    //Publish odom
    odom_pub = n_.advertise<nav_msgs::Odometry>("/odom", 10);


    //Subscibe PLC data
    sub_ = n_.subscribe("/PLC/read", 100, &SubscribeAndPublish::callback, this);
    sub2_ = n_.subscribe("/PLC/read", 100, &SubscribeAndPublish::odomcallback, this);


  }

  void callback(const mc_msgs::readPLC::ConstPtr& msg)
  {
    current_x_pos = msg->X_pos;
    //printf("\t current_x_pos:(%f)\n", current_x_pos);
    current_time = ros::Time::now().toSec();
    //current_time = ros::Time::now().toNSec();
    //ros::Rate r(5); // 5 hz
    //ros::spinOnce();
    //r.sleep();
  }

  void odomcallback(const mc_msgs::readPLC::ConstPtr& msg)
  {
     //ros::Rate r(10); // 10 hz
     //while (ros::ok())
     //{
     //   r.sleep();
     //   break;
    //}
    //loop_rate.sleep();
    tf::TransformBroadcaster broadcaster;
    last_time_x_pos = msg->X_pos;


    //delay(100);
    last_time = ros::Time::now().toSec();
    //last_time = ros::Time::now().toNSec();
    printf("\t current_x_pos:(%f), last_time_x_pos:(%f)\n", current_x_pos, last_time_x_pos);
    //current_time = ros::Time::now().toSec();
    double dt = -(current_time - last_time);
    //float dt = 0.1;
    //
    printf("\t current_time:(%f), last_time:(%f), dt:(%f)\n", current_time, last_time, dt);
    //
    //vy = (last_time_x_pos-current_x_pos)/1000/dt;
    vy = (last_time_x_pos-current_x_pos)/1000/dt;
    //
    double xd= last_time_x_pos-current_x_pos;
    printf("\t xd:(%f), dt:(%f), vy:(%f)\n", xd, dt, vy);
    // message declarations
    //geometry_msgs::TransformStamped odom_trans;
    //odom_trans.header.frame_id = "odom";
    //odom_trans.child_frame_id = "base_link";
    
    //vy=vy+0.1;
    //current_time = ros::Time::now().toSec();

    //double dt = (current_time - last_time).toSec();
    //float delta_x = (vx * cos(th) - vy * sin(th)) * dt;
    //float delta_y = (vx * sin(th) + vy * cos(th)) * dt;
    //float delta_th = vth * dt;

    x = 0;

    y = current_x_pos/1000;
    th =0;

    geometry_msgs::Quaternion odom_quat;	
    odom_quat = tf::createQuaternionMsgFromRollPitchYaw(0,0,th);

    // update transform
    //odom_trans.header.stamp = current_time;
    odom_trans.header.stamp =ros::Time::now();
    odom_trans.transform.translation.x = x; 
    odom_trans.transform.translation.y = y; 
    odom_trans.transform.translation.z = 0.0;
    odom_trans.transform.rotation = tf::createQuaternionMsgFromYaw(th);

    //filling the odometry
    nav_msgs::Odometry odom;
    //odom.header.stamp = current_time;
    odom.header.stamp = ros::Time::now();
    odom.header.frame_id = "odom";
    odom.child_frame_id = "base_link";

    // position
    odom.pose.pose.position.x = x;
    odom.pose.pose.position.y = y;
    odom.pose.pose.position.z = 0.0;
    odom.pose.pose.orientation = odom_quat;

    //velocity
    odom.twist.twist.linear.x = vx;
    odom.twist.twist.linear.y = vy;
    odom.twist.twist.linear.z = 0.0;
    odom.twist.twist.angular.x = 0.0;
    odom.twist.twist.angular.y = 0.0;
    odom.twist.twist.angular.z = vth;

    //last_time = current_time;

    // publishing the odometry and the new tf
    broadcaster.sendTransform(odom_trans);

    odom_pub.publish(odom);

    //ros::Rate r(5); // 5 hz
    //ros::spinOnce();
    //r.sleep();
  }

};//End of class SubscribeAndPublish

int main(int argc, char **argv)
{
  //Initiate ROS
  ros::init(argc, argv, "plc_publisher");
  ros::NodeHandle private_nh("~");
  //current_time = ros::Time::now().toSec();
  //last_time = ros::Time::now().toSec();

  //Create an object of class SubscribeAndPublish that will take care of everything
  SubscribeAndPublish SAPObject;

  ros::spin();

  return 0;
}

