#include <string>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>

/*
void plc_callback(const mc_msgs::readPLC::ConstPtr& msg)
{
    ROS_INFO("I have received: [%d] [%d]", msg->X_pos, msg->Y_pos);
}
*/


int main(int argc, char** argv) {

		ros::init(argc, argv, "state_publisher");
		ros::NodeHandle n;
                ros::NodeHandle nh;
                ros::NodeHandle private_nh("~");
		ros::Publisher odom_pub = n.advertise<nav_msgs::Odometry>("odom", 10);
                //ros::Subscriber subpos = nh.subscribe("/PLC/read", 100, plc_callback);


		// initial position
		double x = 0.0; 
		double y = 0.0;
		double th = 0.0;

		// velocity
		double vx = 0.0;
                double vy = 0.0;
                double vth = 0.0;

        //while (ros::ok()) {

		ros::Time current_time;
		ros::Time last_time;
		current_time = ros::Time::now();
		last_time = ros::Time::now();

		tf::TransformBroadcaster broadcaster;
		ros::Rate loop_rate(20);

		const double degree = M_PI/180;

		// message declarations
		geometry_msgs::TransformStamped odom_trans;
		odom_trans.header.frame_id = "odom";
		odom_trans.child_frame_id = "base_link";

        while (ros::ok()) { //ros::ok()
                //vy=vy+0.1;
                //ros::Subscriber subpos = nh.subscribe("/PLC/read", 100, plc_callback);
////////////////////////////////////////////////////////////////////
		current_time = ros::Time::now(); 

		double dt = (current_time - last_time).toSec();
		double delta_x = (vx * cos(th) - vy * sin(th)) * dt;
		//double delta_x = 0.1;
		double delta_y = (vx * sin(th) + vy * cos(th)) * dt;
		double delta_th = vth * dt;

		x += delta_x;
		y += delta_y;
		th += delta_th;

		geometry_msgs::Quaternion odom_quat;	
		odom_quat = tf::createQuaternionMsgFromRollPitchYaw(0,0,th);

		// update transform
		odom_trans.header.stamp = current_time; 
		odom_trans.transform.translation.x = x; 
		odom_trans.transform.translation.y = y; 
		odom_trans.transform.translation.z = 0.0;
		odom_trans.transform.rotation = tf::createQuaternionMsgFromYaw(th);

		//filling the odometry
		nav_msgs::Odometry odom;
		odom.header.stamp = current_time;
		odom.header.frame_id = "odom";
                odom.child_frame_id = "base_link";

		// position
		odom.pose.pose.position.x = x;
		odom.pose.pose.position.y = y;
		odom.pose.pose.position.z = 0.0;
		odom.pose.pose.orientation = odom_quat;

		//velocity
		odom.twist.twist.linear.x = vx;
		odom.twist.twist.linear.y = vy;
		odom.twist.twist.linear.z = 0.0;
		odom.twist.twist.angular.x = 0.0;
		odom.twist.twist.angular.y = 0.0;
		odom.twist.twist.angular.z = vth;

		last_time = current_time;

		// publishing the odometry and the new tf
		broadcaster.sendTransform(odom_trans);
		odom_pub.publish(odom);

                loop_rate.sleep();
        }


        ros::spin();
	return 0;
}

