cmake_minimum_required(VERSION 2.8.3)
project(emergency_msg)

## Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)



## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
    std_msgs
    message_generation
    roscpp
    geometry_msgs
)

add_message_files(
    DIRECTORY msg
    FILES

    detectN.msg
)

## Generate added messages and services with any dependencies listed here
 generate_messages(
   DEPENDENCIES
   std_msgs
   geometry_msgs
 )


catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES cona_msgs
  CATKIN_DEPENDS std_msgs geometry_msgs message_runtime
#  DEPENDS system_lib
)

include_directories(
 include
  ${catkin_INCLUDE_DIRS}
)

install(DIRECTORY msg/
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/msg
  FILES_MATCHING PATTERN "*.msg"
)
