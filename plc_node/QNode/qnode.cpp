
#include "qnode.hpp"

#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <std_msgs/String.h>

#include <tf/tf.h>
//#include <tf/LinearMath/Transform.h>

#include <sstream>

#include <QDebug>


QNode::QNode(int argc, char** argv ) :
    init_argc(argc),
    init_argv(argv)
{
    init();
}

QNode::~QNode()
{
    if(ros::isStarted())
    {
      ros::shutdown(); // explicitly needed since we use ros::start();
      ros::waitForShutdown();
    }
    wait();
}

bool QNode::init() {
    ros::init(init_argc,init_argv,"server_node");
    if ( ! ros::master::check() )
    {
        return false;
    }
    ros::start(); // explicitly needed since our nodehandle is going out of scope.
    ros::NodeHandle n;

    pub_read_data = n.advertise<mc_msgs::readPLC>("/PLC/read",10, this);//1000 60
    sub_write_data = n.subscribe("/PLC/write", 1, &QNode::write_callback, this);//1000 60

    start();
    return true;
}

void QNode::run() {
    ros::Rate loop_rate(100);
    //5hz, 10 30 60

    while ( ros::ok() )
    {
        switch(cnt_)
        {
        case 0:
        {
            int64_t startPnt = START_READ;
            int16_t lenRead = 7;
            emit plc_req(1,startPnt,lenRead);
            cnt_ = 1;
        }
            break;
        case 1:
        {
            int64_t startPnt = START_READ;
            int16_t lenRead = 7;
            emit plc_req(2,startPnt,lenRead);
            cnt_ = 2;
        }
            break;
        case 2:
        {
            int64_t startPnt = START_WRITE1;
            int16_t lenWrite = 7;
            emit plc_write(1,startPnt, lenWrite, x1_, y1_, a1_, st1_);
            cnt_ = 3;
        }
            break;
        case 3:
        {
            int64_t startPnt = START_WRITE2;
            int16_t lenWrite = 7;
            emit plc_write(2,startPnt, lenWrite, en_, es_, x2_, st2_);
            cnt_ = 0;
        }
            break;
        }

        loop_rate.sleep();
        ros::spinOnce();
    }
    Q_EMIT rosShutdown(); // used to signal the gui for a shutdown (useful to roslaunch)

    ros::shutdown();
}

void QNode::write_callback(const mc_msgs::sendPLC::ConstPtr& msg)
{
    en_ = msg->EN;
    es_ = msg->ES;
}

void QNode::pub_plc(qint8 id,qint32 data_X,qint32 data_Y,qint32 data_A,qint16 data_S)
{
    mc_msgs::readPLC mcRead;
    mcRead.id = id;
    mcRead.X_pos = data_X;
    mcRead.Y_pos = data_Y;
    mcRead.Ang = data_A;
    mcRead.start = data_S;
    if(id == 1)
    {
        x1_ = data_X;
        y1_ = data_Y;
        a1_ = data_A;
        st1_ = data_S;
    }
    if(id == 2)
    {
        x2_ = data_X;
        st2_ = data_S;
    }
    pub_read_data.publish(mcRead);
}







