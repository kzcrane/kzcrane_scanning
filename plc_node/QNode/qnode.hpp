#ifndef protocol_sample_QNODE_HPP_
#define protocol_sample_QNODE_HPP_

#ifndef Q_MOC_RUN

#include <QStringListModel>
#include <QThread>
#include <string>

#endif
#include <ros/network.h>
#include <ros/ros.h>

#include <mc_msgs/readPLC.h>
#include <mc_msgs/sendPLC.h>

#define WRITE_PORT1 1600 // 1601
#define READ_PORT1 1601  // 1600
#define WRITE_PORT2 1602
#define READ_PORT2 1603

#define START_WRITE1 6380
#define START_WRITE2 370
#define START_READ 6391 // 6390

using namespace std;

class QNode : public QThread
{
  Q_OBJECT

public:
  QNode(int argc, char** argv);
  virtual ~QNode();

  bool init();
  void run();

  void write_callback(const mc_msgs::sendPLC::ConstPtr& msg);

Q_SIGNALS:
  void rosShutdown();
  void plc_write(qint8 id, qint64 start_w, qint16 len_w, qint32 x, qint32 y,
                 qint32 th, qint16 start);
  void plc_req(qint8 id, qint64 start_r, qint16 len_r);

public Q_SLOTS:
  void pub_plc(qint8, qint32, qint32, qint32, qint16);

private:
  int init_argc;
  char** init_argv;

  void writeData();

  int8_t cnt_ = 0;

  int32_t en_ = 0;
  int32_t es_ = 0;
  int32_t x1_ = 0;
  int32_t x2_ = 0;
  int32_t y1_ = 0;
  int32_t a1_ = 0;
  int32_t st1_ = 0;
  int32_t st2_ = 0;

  ros::Publisher pub_read_data;
  ros::Subscriber sub_write_data;
};

#endif /* protocol_sample_QNODE_HPP_ */
