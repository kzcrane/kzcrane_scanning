
#include <QtGui>
#include <QApplication>

#include "udpManager.h"

int main(int argc, char **argv) {

    QCoreApplication app(argc, argv);

    udpManager w(argc,argv);
    QObject::connect(&w, SIGNAL(finished()), &app, SLOT(quit()));
        
    int result = app.exec();

	return result;
}
