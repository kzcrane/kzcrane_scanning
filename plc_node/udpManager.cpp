#include <QDebug>
#include <udpManager.h>

udpManager::udpManager(int argc, char** argv, QObject* parent)
    : QObject(parent), plc_server(new m_Udp(this)),
      qnode(new QNode(argc, argv))
{
  setObjectName(QString("PLC Manager"));
  QObject::connect(
      plc_server,
      SIGNAL(read_data(qint8, QByteArray, QByteArray, QByteArray, QByteArray)),
      this,
      SLOT(read_data(qint8, QByteArray, QByteArray, QByteArray, QByteArray)));
  QObject::connect(
      qnode,
      SIGNAL(plc_write(qint8, qint64, qint16, qint32, qint32, qint32, qint16)),
      plc_server,
      SLOT(plc_write(qint8, qint64, qint16, qint32, qint32, qint32, qint16)));
  QObject::connect(qnode, SIGNAL(plc_req(qint8, qint64, qint16)), plc_server,
                   SLOT(plc_req(qint8, qint64, qint16)));

  QObject::connect(qnode, SIGNAL(rosShutdown()), this, SLOT(close()));
}

udpManager::~udpManager() { deleteLater(); }

void udpManager::close() { emit finished(); }

void udpManager::read_data(qint8 id, QByteArray dataX, QByteArray dataY,
                          QByteArray dataA, QByteArray dataS)
{

  int32_t X = QByteToInt(dataX.mid(0, 4));
  int32_t Y = QByteToInt(dataY.mid(0, 4));
  int32_t A = QByteToInt(dataA.mid(0, 4));
  int16_t S = QByteToInt(dataS.mid(0, 2));

  qnode->pub_plc(id, X, Y, A, S);
}
