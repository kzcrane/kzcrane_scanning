#include "m_udp.h"

m_Udp::m_Udp(QObject* parent) : QObject(parent)
{
  socket1 = new QUdpSocket(this);
  socket2 = new QUdpSocket(this);
  if (socket1->bind(host_ip, READ_PORT1))
    connect(socket1, SIGNAL(readyRead()), this, SLOT(readyRead1()));
  else
    qDebug() << "bind 1 fail";
  if (socket2->bind(host_ip, READ_PORT2))
    connect(socket2, SIGNAL(readyRead()), this, SLOT(readyRead2()));
  else
    qDebug() << "bind  2fail";

  //  if (socket->bind(host_ip, READ_PORT))
  //  {
  //    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
  //  }
  //  else
  //    qDebug() << "bind fail";
}

void m_Udp::plc_write(qint8 id, qint64 start_w, qint16 len_w, qint32 x,
                      qint32 y, qint32 a, qint16 start)
{
  write_plc(id, 0, "D", start_w, len_w, x, y, a, start);
}

void m_Udp::plc_req(qint8 id, qint64 start_r, qint16 len_r)
{
  req_plc(id, 0, "D", start_r, len_r);
}

int8_t m_Udp::GetDeviceCode(QString strDev)
{
  if (strDev == QString::fromStdString("SM"))
    return 0x91;
  else if (strDev == QString::fromStdString("SD"))
    return 0xA9;
  else if (strDev == QString::fromStdString("X"))
    return 0x9C;
  else if (strDev == QString::fromStdString("Y"))
    return 0x9D;
  else if (strDev == QString::fromStdString("M"))
    return 0x90;
  else if (strDev == QString::fromStdString("L"))
    return 0x92;
  else if (strDev == QString::fromStdString("F"))
    return 0x93;
  else if (strDev == QString::fromStdString("V"))
    return 0x94;
  else if (strDev == QString::fromStdString("B"))
    return 0xA0;
  else if (strDev == QString::fromStdString("D"))
    return 0xA8;
  else if (strDev == QString::fromStdString("W"))
    return 0xB4;
  else
    return -1;
}

void m_Udp::write_plc(qint8 id, qint16 dataType, QString strDevice,
                      qint64 startPoint, qint16 nLen, qint32 data1,
                      qint32 data2, qint32 data3, qint16 data4)
{

  //  int8_t nCode = GetDeviceCode(strDevice);
  int8_t nCode = 0xA8;
  int8_t netNo = 0;
  int8_t plcNo = 255;
  int8_t staNo = 0;
  qint16 reqDataLen = 12 + nLen * 2;
  qint16 monitorTime = 16;

  // Header

  QByteArray SUB_HEADER, NET_NO, PLC_NO, TARG_NO, STA_NO;
  SUB_HEADER.resize(2);
  NET_NO.resize(1);
  PLC_NO.resize(1);
  STA_NO.resize(1);
  TARG_NO.resize(2);

  SUB_HEADER[1] = 0x00;
  SUB_HEADER[0] = 0x50;
  NET_NO[0] = netNo;
  PLC_NO[0] = plcNo;
  STA_NO[0] = staNo;
  TARG_NO[0] = 0xFF;
  TARG_NO[1] = 0x03;

  QByteArray REQ_LEN, MONITOR_TIME;
  REQ_LEN.resize(2);
  MONITOR_TIME.resize(2);
  REQ_LEN = shortTo2Byte(reqDataLen);
  MONITOR_TIME = shortTo2Byte(monitorTime);

  // Request Data

  QByteArray BATCH_WRITE, DATA_TYPE;
  BATCH_WRITE.resize(2);
  DATA_TYPE.resize(2);
  BATCH_WRITE[0] = 0x01;
  BATCH_WRITE[1] = 0x14;

  qint16 data_type = 0;
  if (strDevice == "X" || strDevice == "Y")
    data_type = 1;
  else
    data_type = 0;
  DATA_TYPE = shortTo2Byte(data_type); // 0: word, 1: bit

  QByteArray START_POINT;
  START_POINT.resize(3);
  START_POINT = intTo3Byte(startPoint);

  QByteArray NCODE;
  NCODE.resize(1);
  NCODE[0] = nCode;

  QByteArray NLEN;
  NLEN.resize(4);
  NLEN = intTo4Byte(nLen);

  QByteArray DATA1;
  DATA1.resize(4);
  DATA1 = intTo4Byte(data1);

  QByteArray DATA2;
  DATA2.resize(4);
  DATA2 = intTo4Byte(data2);

  QByteArray DATA3;
  DATA3.resize(4);
  DATA3 = intTo4Byte(data3);

  QByteArray DATA4;
  DATA4.resize(2);
  DATA4 = shortTo2Byte(data4);

  QByteArray Data;
  Data.append(SUB_HEADER);
  Data.append(NET_NO);
  Data.append(PLC_NO);
  Data.append(TARG_NO);
  Data.append(STA_NO);
  Data.append(REQ_LEN);
  Data.append(MONITOR_TIME);
  Data.append(BATCH_WRITE);
  Data.append(DATA_TYPE);
  Data.append(START_POINT);
  Data.append(NCODE);
  Data.append(NLEN);
  Data.append(DATA1);
  Data.append(DATA2);
  Data.append(DATA3);
  Data.append(DATA4);

  // Data send = 14

  if (id == 1)
  {
    socket1->writeDatagram(Data, plc_ip1, WRITE_PORT1);
//    qDebug() << "write PLC 1"
//             << ": X: " << data1 << ", Y: " << data2 << ", A: " << data3
//             << ", Start:" << data4;
  }
  else if (id == 2)
  {
    socket2->writeDatagram(Data, plc_ip2, WRITE_PORT2);
//    qDebug() << "write PLC 2"
//             << ": EN: " << data1 << ", ES: " << data2 << ", X: " << data3
//             << ", Start:" << data4;
    ;
  }
}

void m_Udp::req_plc(qint8 id, qint16 dataType, QString strDevice,
                    qint64 startPoint, qint16 nLen)
{

  //  int8_t nCode = GetDeviceCode(strDevice);
  int8_t nCode = 0xA8;
  int8_t netNo = 0;
  int8_t plcNo = 255;
  int8_t staNo = 0;
  qint16 reqDataLen = 12;
  qint16 monitorTime = 16;

  // Header

  QByteArray SUB_HEADER, NET_NO, PLC_NO, TARG_NO, STA_NO;
  SUB_HEADER.resize(2);
  NET_NO.resize(1);
  PLC_NO.resize(1);
  STA_NO.resize(1);
  TARG_NO.resize(2);

  SUB_HEADER[1] = 0;
  SUB_HEADER[0] = 0x50;
  NET_NO[0] = netNo;
  PLC_NO[0] = plcNo;
  STA_NO[0] = staNo;
  TARG_NO[0] = 0xFF;
  TARG_NO[1] = 0x03;

  QByteArray REQ_LEN, MONITOR_TIME;
  REQ_LEN.resize(2);
  MONITOR_TIME.resize(2);
  REQ_LEN = shortTo2Byte(reqDataLen);
  MONITOR_TIME = shortTo2Byte(monitorTime);

  // Request Data

  QByteArray BATCH_READ, DATA_TYPE;
  BATCH_READ.resize(2);
  DATA_TYPE.resize(2);
  BATCH_READ[0] = 0x01;
  BATCH_READ[1] = 0x04;
  qint16 data_type = 0;
  if (strDevice == "X" || strDevice == "Y")
    data_type = 1;
  else
    data_type = 0;
  DATA_TYPE = shortTo2Byte(data_type); // 0: word, 1: bit

  QByteArray START_POINT;
  START_POINT.resize(3);
  START_POINT = intTo3Byte(startPoint);

  QByteArray NCODE;
  NCODE.resize(1);
  NCODE[0] = nCode;

  QByteArray NLEN;
  NLEN.resize(2);
  NLEN = shortTo2Byte(nLen);

  QByteArray Data;
  Data.append(SUB_HEADER);
  Data.append(NET_NO);
  Data.append(PLC_NO);
  Data.append(TARG_NO);
  Data.append(STA_NO);
  Data.append(REQ_LEN);
  Data.append(MONITOR_TIME);
  Data.append(BATCH_READ);
  Data.append(DATA_TYPE);
  Data.append(START_POINT);
  Data.append(NCODE);
  Data.append(NLEN);

  // qDebug() << "request";

  if (id == 1)
  {
    socket1->writeDatagram(Data, plc_ip1, WRITE_PORT1);
//    qDebug() << "request PLC 1";
  }
  else if (id == 2)
  {
    socket2->writeDatagram(Data, plc_ip2, WRITE_PORT2);
//    qDebug() << "request PLC 2";
  }
}

void m_Udp::readyRead1()
{

  QHostAddress sender;
  quint16 senderPort;

  qDebug() << "read 1";

  while (socket1->hasPendingDatagrams())
  {
    QByteArray buffer;
    buffer.resize(static_cast<qint32>(socket1->pendingDatagramSize()));
    socket1->readDatagram(buffer.data(), buffer.size(), &sender, &senderPort);

    qDebug() << "read: ip" << sender.toString() << "port" << senderPort;

    //  if (senderPort == WRITE_PORT1)
    //    return;

    QByteArray STX1, STX2, NET_NO, PLC_NO, TARG1, TARG2, STA_NO, RES_LEN1,
        RES_LEN2;
    if (sender == plc_ip1 && senderPort == READ_PORT1)
    {
      STX1 = buffer.mid(0, 1);
      STX2 = buffer.mid(1, 1);
      NET_NO = buffer.mid(2, 1);
      PLC_NO = buffer.mid(3, 1);
      TARG1 = buffer.mid(4, 1);
      TARG2 = buffer.mid(5, 1);
      STA_NO = buffer.mid(6, 1);
      RES_LEN1 = buffer.mid(7, 1);
      RES_LEN2 = buffer.mid(8, 1);

      if ((STX1.toHex() == "d0") && (STX2.toHex() == "00") &&
          (NET_NO.toHex() == "00") && (PLC_NO.toHex() == "ff"))
      {

        if (buffer.size() == 25)
        {
          emit read_data(1, buffer.mid(11, 4), buffer.mid(15, 4),
                         buffer.mid(19, 4), buffer.mid(23, 2));
        }
      }
    }
  }
}

void m_Udp::readyRead2()
{

  qDebug() << "read 2";
  QHostAddress sender;
  quint16 senderPort;

  while (socket2->hasPendingDatagrams())
  {
    QByteArray buffer;
    buffer.resize(static_cast<qint32>(socket2->pendingDatagramSize()));
    socket2->readDatagram(buffer.data(), buffer.size(), &sender, &senderPort);

    qDebug() << "read: ip" << sender << "port" << senderPort;

    //  if (senderPort == WRITE_PORT2)
    //    return;

    QByteArray STX1, STX2, NET_NO, PLC_NO, TARG1, TARG2, STA_NO, RES_LEN1,
        RES_LEN2;

    if (sender == plc_ip2 && senderPort == READ_PORT2)
    {
      STX1 = buffer.mid(0, 1);
      STX2 = buffer.mid(1, 1);
      NET_NO = buffer.mid(2, 1);
      PLC_NO = buffer.mid(3, 1);
      TARG1 = buffer.mid(4, 1);
      TARG2 = buffer.mid(5, 1);
      STA_NO = buffer.mid(6, 1);
      RES_LEN1 = buffer.mid(7, 1);
      RES_LEN2 = buffer.mid(8, 1);

      if ((STX1.toHex() == "d0") && (STX2.toHex() == "00") &&
          (NET_NO.toHex() == "00") && (PLC_NO.toHex() == "ff"))
      {
        // qDebug()<<"read data with size" << buffer.size();
        // emit read_data(buffer.mid(11,4), buffer.mid(15,4), buffer.mid(19,4),
        // buffer.mid(23,2));

        if (buffer.size() == 25)
        {
          emit read_data(2, buffer.mid(11, 4), buffer.mid(15, 4),
                         buffer.mid(19, 4), buffer.mid(23, 2));
        }
      }
    }
  }
}
