#ifndef M_UDP_H
#define M_UDP_H

#define host_ip QHostAddress("192.168.10.52")
#define plc_ip1 QHostAddress("192.168.10.11")
#define plc_ip2 QHostAddress("192.168.10.12")

#define WRITE_PORT1 1600 // 1601
#define READ_PORT1 1601  // 1600
#define WRITE_PORT2 1602
#define READ_PORT2 1603

#define START_WRITE1 6380
#define START_WRITE2 370
#define START_READ 6391 // 6390

#include <QByteArray>
#include <QObject>
#include <QUdpSocket>
#include <convertToByte.hpp>
#include <math.h>
#include <string>

class m_Udp : public QObject
{
  Q_OBJECT
public:
  explicit m_Udp(QObject* parent = 0);


signals:
  void read_data(qint8, QByteArray, QByteArray, QByteArray, QByteArray);

public slots:
  void readyRead1();
  void readyRead2();
  void plc_write(qint8 id, qint64 start_w, qint16 len_w, qint32 x, qint32 y,
                 qint32 th, qint16 start);
  void plc_req(qint8 id, qint64 start_r, qint16 len_r);

private:
  int8_t GetDeviceCode(QString strDev);
  QUdpSocket* socket1;
  QUdpSocket* socket2;
  void req_plc(qint8 id, qint16 dataType, QString strDevice, qint64 startPoint,
               qint16 nLen);
  void write_plc(qint8 id, qint16 dataType, QString strDevice,
                 qint64 startPoint, qint16 nLen, qint32 dataX, qint32 dataY,
                 qint32 dataA, qint16 dataS);
};

#endif // M_UDP_H
