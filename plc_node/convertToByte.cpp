#include <convertToByte.hpp>

QByteArray shortTo2Byte(int16_t data)
{
    QByteArray qba(reinterpret_cast<const char *>(&data), sizeof(short));

    QByteArray buffer;
    buffer.resize(2);
    buffer[0]=qba[0];
    buffer[1]=qba[1];

    return buffer;
}

int16_t QByteToShort(QByteArray byte)
{
//(int16_t)buffer.toHex().toUShort(nullptr,16)
    QByteArray byte_inv;
    byte_inv.resize(2);
    byte_inv[0] = byte[1];
    byte_inv[1] = byte[0];
    return static_cast<int16_t>(byte_inv.toHex().toUShort(nullptr,16));
}

int8_t QByteToInt8(QByteArray byte)
{
//(int16_t)buffer.toHex().toUShort(nullptr,16)
    return static_cast<int8_t>(byte.toHex().toUShort(nullptr,8));
}

QByteArray doubleTo8Byte(qreal data)
{
    QByteArray qba(reinterpret_cast<const char *>(&data), sizeof(double));

    QByteArray buffer;
    buffer.resize(8);
    buffer[7]=qba[0];
    buffer[6]=qba[1];
    buffer[5]=qba[2];
    buffer[4]=qba[3];
    buffer[3]=qba[4];
    buffer[2]=qba[5];
    buffer[1]=qba[6];
    buffer[0]=qba[7];

    return buffer;
}

double QByteToDouble(QByteArray byte)
{
    return static_cast<double>(byte.toHex().toDouble(nullptr));
}

QByteArray intTo4Byte(int32_t data)
{
    QByteArray qba(reinterpret_cast<const char *>(&data), sizeof(int32_t));

    QByteArray buffer;
    buffer.resize(4);
    buffer[3]=qba[3];
    buffer[2]=qba[2];
    buffer[1]=qba[1];
    buffer[0]=qba[0];

    return buffer;
}

QByteArray intTo3Byte(int64_t data)
{
    QByteArray qba(reinterpret_cast<const char *>(&data), sizeof(int64_t));

    QByteArray buffer;
    buffer.resize(3);
    buffer[2]=qba[2];
    buffer[1]=qba[1];
    buffer[0]=qba[0];

    return buffer;
}

QByteArray longTo8Byte(int64_t data)
{
    QByteArray qba(reinterpret_cast<const char *>(&data), sizeof(int64_t));

    QByteArray buffer;
    buffer.resize(8);
    buffer[7]=qba[0];
    buffer[6]=qba[1];
    buffer[5]=qba[2];
    buffer[4]=qba[3];
    buffer[3]=qba[4];
    buffer[2]=qba[5];
    buffer[1]=qba[6];
    buffer[0]=qba[7];

    return buffer;
}

int64_t QByteToLong(QByteArray byte)
{
   return static_cast<int64_t>(byte.toHex().toULong(nullptr,16));
}

int32_t QByteToInt(QByteArray byte)
{
    QByteArray byte_inv;
    byte_inv.resize(4);
    byte_inv[0] = byte[3];
    byte_inv[1] = byte[2];
    byte_inv[2] = byte[1];
    byte_inv[3] = byte[0];
    return static_cast<int32_t>(byte_inv.toHex().toUInt(nullptr,16));
}
