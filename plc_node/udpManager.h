#ifndef udp_MANAGER_H
#define udp_MANAGER_H

#include <QtCore>
#include <QObject>

#include <UDP/m_udp.h>
#include <QNode/qnode.hpp>

class udpManager : public QObject
{
    Q_OBJECT
    
public:
//    explicit 
    udpManager(int argc, char** argv, QObject *parent = nullptr);
    ~udpManager();
	
public slots:
    void close();
    void read_data(qint8, QByteArray, QByteArray, QByteArray, QByteArray);
    

signals:
    void finished();

private:
    m_Udp *plc_server;
    QNode *qnode;
};

#endif
