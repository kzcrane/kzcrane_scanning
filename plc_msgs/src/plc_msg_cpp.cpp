#include "ros/ros.h"
#include "plc_msgs/plc_pub_msg.h"
#include <sstream>
#include "mc_msgs/readPLC.h"

int x1_pos;
int y1_pos;

int x2_pos;
int y2_pos;

float x_speed;
float y_speed;

/*
void plc_callback1(const mc_msgs::readPLC::ConstPtr& msg)
{
    ROS_INFO("I have received x1, y1: [%d] [%d]", msg->X_pos, msg->Y_pos);
    x1_pos=msg->X_pos;
    y1_pos=msg->Y_pos;
}

void plc_callback2(const mc_msgs::readPLC::ConstPtr& msg)
{
    ROS_INFO("I have received x2, y2: [%d] [%d]", msg->X_pos, msg->Y_pos);
    x2_pos=msg->X_pos;
    y2_pos=msg->Y_pos;


}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "plc_msgs");
 	ros::NodeHandle n;
        ros::NodeHandle nh;
        ros::NodeHandle private_nh("~");
        //ros::Publisher plc_pub = n.advertise<plc_msgs::plc_pub_msg>("plc_pub", 10);
        ros::Subscriber subpos1 = nh.subscribe("/PLC/read", 100, plc_callback1);
        ros::Subscriber subpos2= nh.subscribe("/PLC/read", 100, plc_callback2);

        //plc_msgs::plc_pub_msg msg;
        //msg.x_pos=x2_pos;
        //msg.y_pos=y2_pos;
        //msg.x_speed=1;
        //msg.y_speed=1;

        //plc_pub.publish(msg);
        ROS_INFO("publish: [%d] [%d] [%f] [%f] ", msg.x_pos, msg.y_pos, msg.x_speed, msg.y_speed);
        ros::spin();
	return 0;
}
*/

class SubscribeAndPublish
{
public:
  SubscribeAndPublish()
  {
    //Topic to publish pcl_pub_msg
    pub_ = n_.advertise<plc_msgs::plc_pub_msg>("/plc_pub", 1);

    //Topic to subscribe /PLC/read
    sub_ = n_.subscribe("/PLC/read", 100, &SubscribeAndPublish::callback, this);
  }

  void callback(const mc_msgs::readPLC::ConstPtr& msg)
  {
    plc_msgs::plc_pub_msg pub_msg;
    //.... do something with the input and generate the output...
    pub_msg.x_pos=msg->X_pos;
    pub_msg.y_pos=msg->Y_pos;
    pub_msg.x_speed=1;
    pub_msg.y_speed=1;
    pub_.publish(pub_msg);
  }

private:
  ros::NodeHandle n_;
  ros::Publisher pub_;
  ros::Subscriber sub_;

};//End of class SubscribeAndPublish

int main(int argc, char **argv)
{
  //Initiate ROS
  ros::init(argc, argv, "subscribe_and_publish");

  //Create an object of class SubscribeAndPublish that will take care of everything
  SubscribeAndPublish SAPObject;

  ros::spin();

  return 0;
}
