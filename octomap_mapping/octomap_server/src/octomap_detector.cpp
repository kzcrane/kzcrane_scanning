#include <ros/ros.h>
#include <octomap_server/OctomapServer.h>
#include <visualization_msgs/MarkerArray.h>

#include <visualization_msgs/Marker.h>


#include <nav_msgs/OccupancyGrid.h>
#include <std_msgs/ColorRGBA.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_srvs/Empty.h>
#include <dynamic_reconfigure/server.h>
#include <octomap_server/OctomapServerConfig.h>

#include <pcl/point_types.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/sample_consensus/method_types.h>
#include <ros/ros.h>
#include <octomap_server/OctomapServer.h>
#include <visualization_msgs/MarkerArray.h>

#include <visualization_msgs/Marker.h>


#include <nav_msgs/OccupancyGrid.h>
#include <std_msgs/ColorRGBA.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_srvs/Empty.h>
#include <dynamic_reconfigure/server.h>
#include <octomap_server/OctomapServerConfig.h>

#include <pcl/point_types.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/sample_consensus/method_types.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"  // pcl::SAC_SAMPLE_SIZE is protected since PCL 1.8.0
#include <pcl/sample_consensus/model_types.h>
#pragma GCC diagnostic pop

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl_conversions/pcl_conversions.h>


#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
#include <octomap_msgs/Octomap.h>
#include <octomap_msgs/GetOctomap.h>
#include <octomap_msgs/BoundingBoxQuery.h>
#include <octomap_msgs/conversions.h>

#include <octomap_ros/conversions.h>
#include <octomap/octomap.h>
#include <octomap/OcTreeKey.h>


#include <octomap/AbstractOcTree.h>


/*
#include <boost/memory.hpp>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/mutex.hpp>
*/

//tinyxml
#include <tinyxml.h>
#include <iostream> 

//base64
#include <base64.h>
#include <string.h>


#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <stdint.h>

using namespace std;
#include <boost/thread/tss.hpp>

static boost::thread_specific_ptr<string> _tssThreadNameSptr;

#include <base64.h>
#include <vector>


////////pointCloud


#include <pcl_ros/point_cloud.h>
#include <boost/foreach.hpp>

//pointcloudX

#include<algorithm>
#include<iostream>

#include <sstream>

#include <mc_msgs/readPLC.h>

#include "octomap_server/OctomapServer.h"
//
#include  <time.h>

#include <emergency_msg/detectN.h>
typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

//std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len);
//static inline bool is_base64(unsigned char c);
//void xmlmake(int bunkerNum, int size_x, int size_y, int size_z, int visionPeakX, int visionPeakY, int visionPeakZ, int totalPeakX, int totalPeakY, int totalPeakZ, int cells[][3], string data64, double bunkervolume);
//void bunker84xmlmake(int bunkerNum, int size_x, int size_y, int size_z, int visionPeakX, int visionPeakY, int visionPeakZ, int totalPeakX, int totalPeakY, int totalPeakZ, int cells[][3], string data64, double bunkervolume);
//void cloud_cb(const PointCloud::ConstPtr& msg);
void pointcloud_callback (const PointCloud::ConstPtr& msg);
void callback(const PointCloud::ConstPtr& msg);
//void bunkerNumcallback(const mc_msgs::readPLC::ConstPtr& msg);

int bunkerNumber;
int pastbunkerNumber;

int crtbunkerNumber;
int ptbunkerNumber;

int X_pos_3;
int X_pos_d;
int crt_X_pos;
int pt_X_pos;

int Y_pos_3;
int Y_pos_d;
int crt_Y_pos;
int pt_Y_pos;

bool bunkerxml=false;
bool reset_bool=false;

int scanner102 = 5000;

int minY=-9500;
int maxY=10500;
bool detect=false;
bool detect1=false;

/*
std::string base64_decode(std::string const& encoded_string);

static const std::string base64_chars = 
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";

typedef union
{
  unsigned char a[4];
  float b;
} union_data;


//출처: https://jsilva.tistory.com/24 [sweetpast]

vector<unsigned char> intToBytes(int paramInt)
{
     vector<unsigned char> arrayOfByte(4);
     for (int i = 0; i < 4; i++)
         arrayOfByte[3 - i] = (paramInt >> (i * 8));
     return arrayOfByte;
}



void callback(const PointCloud::ConstPtr& msg)
{

    long allpoints = (msg->width)*(msg->height);
    long data[3*allpoints];
    long all_Points[allpoints][allpoints][3];
    unsigned char bytesTest[12];

    int i=0;
    int p=0;
    long datanumber=1;
    int bunkerdeep=1000; //3300//1000
    int emergencyNorthDeep = 1000;//400;//2200;//-2000;
    vector<pair<long, long> > v;
    union_data data_s1,data_r1, data_s2,data_r2, data_s3,data_r3;


    //octomap
    BOOST_FOREACH (const pcl::PointXYZ& pt, msg->points)
    {
        //printf ("\t(%f, %f, %f)\n", pt.x, pt.y, pt.z);
        data[p]=pt.y * 1000-scanner102;
        data[p+1]=pt.x * 1000;
        data[p+2]=pt.z * 1000;
        v.push_back(pair<long, long>(data[p], data[p+1]));
        if((data[p+2]>2000) && (-8000<data[p+1]<6000))
        {
            printf("obstacle detect");
        }
        p=p+3;
        datanumber=datanumber+1;

    }
    //x,y sort
    sort(v.begin(), v.end() );
    int k=0;



   pt_X_pos = crt_X_pos;
   pt_Y_pos = crt_Y_pos;
   //ptbunkerNumber = crtbunkerNumber;
   bunkerNumber = 0;



}

void Xposcallback(const mc_msgs::readPLC::ConstPtr& msg)
{
    crt_X_pos = msg -> X_pos;
    crt_X_pos = crt_X_pos; //+ scanner101;
    X_pos_3 = crt_X_pos*0.01;
    crt_X_pos = X_pos_3*100;

    crt_Y_pos = msg -> Y_pos;
    Y_pos_3 = crt_Y_pos*0.01;
    crt_Y_pos = Y_pos_3*100;
    //printf("\t current x %d \n", crt_X_pos);

    //printf("\t bunkerxml:%d \n", bunkerxml);
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "octomap_detector");
  ros::NodeHandle nh;
  ros::NodeHandle private_nh("~");
  ros::Subscriber subX;
  ros::Subscriber sub;
  ros::ServiceClient client;//

  while(ros::ok())
  {
    ros::spinOnce();
    sub = nh.subscribe<PointCloud>("/octomap_point_cloud_centers2", 5000, callback);
  }

}
*/
//////////////////////////////////////////////////////////////////////////////////////
emergency_msg::detectN detectN;

class SubscribeAndPublish
{
private:
    ros::NodeHandle nh;
    ros::Publisher detectN_pub;
    ros::Subscriber subX;
    ros::Subscriber sub;
    //ros::ServiceClient client;
    ros::ServiceClient client =nh.serviceClient<std_srvs::Empty> ("/octomap_server6/reset");
    std_srvs::Empty srv;
    ros::ServiceClient client1 =nh.serviceClient<std_srvs::Empty> ("/octomap_serve1/reset");
    std_srvs::Empty srv1;

public:
    SubscribeAndPublish()
    {
        //Publish detectN
        detectN_pub = nh.advertise<emergency_msg::detectN>("detectN", 100);
        //tf::TransformBroadcaster broadcaster;


        subX = nh.subscribe("/octomap_point_cloud_centers6", 100, &SubscribeAndPublish::callback, this);
        sub = nh.subscribe("/octomap_point_cloud_centers1", 100, &SubscribeAndPublish::callback_scan1, this);
        ros::spin();
    }

    void callback(const PointCloud::ConstPtr& msg)
    {
        long allpoints = (msg->width)*(msg->height);
        long data[3*allpoints];
        //long all_Points[allpoints][allpoints][3];
        unsigned char bytesTest[12];

        int i=0;//int
        int p=0;//int
        long datanumber=1;
        int bunkerdeep=1000; //3300//1000
        int emergencyNorthDeep = 1000;//400;//2200;//-2000;
        vector<pair<long, long> > v;
        //union_data data_s1,data_r1, data_s2,data_r2, data_s3,data_r3;


        //octomap

        BOOST_FOREACH (const pcl::PointXYZ& pt, msg->points)
        {
            //printf ("\t(%f, %f, %f)\n", pt.x, pt.y, pt.z);
            data[p]=pt.y * 1000-scanner102;
            data[p+1]=pt.x * 1000;
            data[p+2]=pt.z * 1000;
            v.push_back(pair<long, long>(data[p], data[p+1]));
            if((data[p+2]>4500) && (-7000<data[p+1]) &&(data[p+1]<8000))
            {
                printf("N x:%d y:%d z:%d obstacle detect \n",data[p], data[p+1],data[p+2]);
                detect=true;
            }
            p=p+3;
            //datanumber=datanumber+1;

        }
        //x,y sort
        sort(v.begin(), v.end() );
        int k=0;



        //emergency_msg::detectN detectN;
        detectN.detect_N = detect;

        client.call(srv);
        //ros::service::call("/octomap_server/reset", srv);
        ros::service::call("/octomap_server6/reset", srv);





        //detectN_pub.publish(detectN);
        //detect = false;


    }

    void callback_scan1(const PointCloud::ConstPtr& msg)
    {
        long allpoints = (msg->width)*(msg->height);
        long data[3*allpoints];
        //long all_Points[allpoints][allpoints][3];
        unsigned char bytesTest[12];

        int i=0;//int
        int p=0;//int
        long datanumber=1;
        int bunkerdeep=1000; //3300//1000
        int emergencyNorthDeep = 1000;//400;//2200;//-2000;
        vector<pair<long, long> > v;
        //union_data data_s1,data_r1, data_s2,data_r2, data_s3,data_r3;


        //octomap


        BOOST_FOREACH (const pcl::PointXYZ& pt, msg->points)
        {
            //printf ("\t(%f, %f, %f)\n", pt.x, pt.y, pt.z);
            data[p]=pt.y * 1000-scanner102;
            data[p+1]=pt.x * 1000;
            data[p+2]=pt.z * 1000;
            v.push_back(pair<long, long>(data[p], data[p+1]));
            if((data[p+2]>4320) && (data[p+2]<9000) && (-11900<data[p+1]) &&(data[p+1]<-3200))
            {
                printf("S x:%d y:%d z:%d obstacle detect \n",data[p], data[p+1],data[p+2]);
                detect1=true;
            }
            if((data[p+2]>4320) && (data[p+2]<9000) && (-2600<data[p+1]) &&(data[p+1]<6400))
            {
                printf("S x:%d y:%d z:%d obstacle detect \n",data[p], data[p+1],data[p+2]);
                detect1=true;
            }
            if((data[p+2]>4320) && (data[p+2]<9000) && (7000<data[p+1]) &&(data[p+1]<14000))
            {
                printf("S x:%d y:%d z:%d obstacle detect \n",data[p], data[p+1],data[p+2]);
                detect1=true;
            }
            p=p+3;
            //datanumber=datanumber+1;

        }
        //x,y sort
        sort(v.begin(), v.end() );
        int k=0;



        emergency_msg::detectN detectN;
        detectN.detect_S = detect1;

        client.call(srv1);
        //ros::service::call("/octomap_server/reset", srv);
        ros::service::call("/octomap_server1/reset", srv);





        detectN_pub.publish(detectN);
        detect1 = false;
        detect = false;


    }



};//End of class SubscribeAndPublish

int main(int argc, char **argv)
{
    //Initiate ROS
    ros::init(argc, argv, "octomap_detector");
    ros::NodeHandle private_nh("~");


    //Create an object of class SubscribeAndPublish that will take care of everything
    SubscribeAndPublish SAPObject;

    //ros::spin();

    return 0;
}


