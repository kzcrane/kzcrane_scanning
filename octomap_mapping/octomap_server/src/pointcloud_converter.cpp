#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>    
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

ros::Publisher pub;

void points_callback(const sensor_msgs::PointCloud2ConstPtr &msg){
    
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);
    pcl::fromROSMsg(*msg,*cloud);    

    pub.publish(cloud);    
}

int main(int argc, char** argv)
{
    ros::init(argc,argv,"pointcloud_converter");
    ros::NodeHandle nh;
    ros::NodeHandle p_nh("~");

    //ros::Subscriber sub;
    ros::Subscriber sub_pointcloud = nh.subscribe("/ambient_pointcloud", 50, points_callback);
    //sub = nh.subscribe<sensor_msgs::PointCloud2>("/ambient_pointcloud",50,points_callback);
    pub = nh.advertise<sensor_msgs::PointCloud2>("plane_points",50);
    
    ros::spin();

    return 0;
}

//출처: https://ahshinyong.tistory.com/entry/pclPointCloudsensormsgsPointCloud2 [while(1)]
