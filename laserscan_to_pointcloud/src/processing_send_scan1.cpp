#include <iostream>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
typedef pcl::PointXYZ PointXYZ;
typedef sensor_msgs::PointCloud2 PointCloud2;

class FPCL
{
    public:
        FPCL()
        {
            pub = nh.advertise<PointCloud> ("/ambient_pointcloud", 1);
            sub = nh.subscribe<PointCloud2> ("/ambient_pointcloud0", 1, &FPCL::callback, this);
            //this->t = 5;
            this->t = 3;
        }

        void callback(const boost::shared_ptr<const sensor_msgs::PointCloud2>& msg)
        {
            pcl::PCLPointCloud2 pcl_pc2;
            pcl_conversions::toPCL(*msg,pcl_pc2);
            PointCloud::Ptr cloud(new PointCloud);
            pcl::fromPCLPointCloud2(pcl_pc2,*cloud);
            PointCloud::Ptr cloud_ft (new PointCloud);
            pcl::VoxelGrid<PointXYZ> vox;
            vox.setInputCloud(cloud);
            //vox.setLeafSize(0.05f, 0.05f, 0.05f);
            //vox.setLeafSize(0.2f, 0.2f, 0.2f);
            //vox.setLeafSize(0.07f, 0.06f, 0.04f);
            //vox.setLeafSize(0.07f, 0.06f, 0.07f);
            //vox.setLeafSize(0.07f, 0.1f, 0.07f);
            //vox.setLeafSize(0.02f, 0.02f, 0.02f);
            vox.setLeafSize(0.02f, 0.02f, 0.02f);
            //vox.setFilterLimits(-1.0, 1.0);
            vox.setFilterLimits(-1.0, 140.0);
            vox.setMinimumPointsNumberPerVoxel(this->t);
            vox.filter(*cloud_ft);

            pub.publish (cloud_ft);
        }

    private:
        ros::NodeHandle nh;
        ros::Publisher pub;
        ros::Subscriber sub;
        int t;

};


int main(int argc, char** argv)
{
    ros::init(argc, argv, "processing_send");
    FPCL f = FPCL();
    ros::spin();
    return 0;
}
