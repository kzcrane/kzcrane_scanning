

#include <pcl/filters/statistical_outlier_removal.h>

#include <iostream>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
typedef pcl::PointXYZ PointXYZ;
typedef sensor_msgs::PointCloud2 PointCloud2;

class FPCL
{
    public:
        FPCL()
        {
            pub = nh.advertise<PointCloud> ("/ambient_pointcloud", 1);
            sub = nh.subscribe<PointCloud2> ("/ambient_pointcloud0", 1, &FPCL::callback, this);
            this->t = 1;
        }

        void callback(const boost::shared_ptr<const sensor_msgs::PointCloud2>& msg)
        {
            pcl::PCLPointCloud2 pcl_pc2;
            pcl_conversions::toPCL(*msg,pcl_pc2);
            PointCloud::Ptr cloud(new PointCloud);
            pcl::fromPCLPointCloud2(pcl_pc2,*cloud);
            PointCloud::Ptr cloud_ft (new PointCloud);

            // Create the filtering object OutlierRemoval
            //
            if(cloud->size()>0)
            //
            {
                pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
                sor.setInputCloud (cloud);
                sor.setMeanK (5);
                sor.setStddevMulThresh (this->t);
                sor.filter (*cloud_ft);




                pub.publish (cloud_ft);
            //
            }
            //
        }

    private:
        ros::NodeHandle nh;
        ros::Publisher pub;
        ros::Subscriber sub;
        int t;

};


int main(int argc, char** argv)
{
    ros::init(argc, argv, "processing_send_test");
    FPCL f = FPCL();
    ros::spin();
    return 0;
}

